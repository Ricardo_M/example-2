/**
 * 
 */
package giosoft.net;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Formatter;

/**
 * @author ryky21
 * 
 */
public class MainNet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args[0] != null && !args[0].equalsIgnoreCase("interfaces")) {
			String keyGen = getBorderColor(args[0]);
			System.out.println(keyGen);
			System.exit(0);
		}

		Enumeration<NetworkInterface> nets;
		try {
			nets = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netIf : Collections.list(nets)) {
				System.out.printf("Nombre Pantalla: %s\n",
						netIf.getDisplayName());
				System.out.printf("Nombre: %s\n", netIf.getName());
				displaySubInterfaces(netIf);
				System.out.printf("\n");
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getBorderColor(String eth) {
		String color = null;
		try {
			NetworkInterface borde = NetworkInterface.getByName(eth);
			Formatter formatter = new Formatter();
			byte[] colorBits = borde.getHardwareAddress();
			color = "";
			for (int i = 0; i < colorBits.length; i++) {
				color += formatter.format("%02X%s", colorBits[i],
						(i < colorBits.length - 1) ? "-" : "");
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return color;
	}

	public static void displaySubInterfaces(NetworkInterface netIf)
			throws SocketException {
		Enumeration<NetworkInterface> subIfs = netIf.getSubInterfaces();

		for (NetworkInterface subIf : Collections.list(subIfs)) {
			System.out.printf("\tSub Nombre Pantalla de Interfaz: %s\n",
					subIf.getDisplayName());
			System.out.printf("\tSub Nombre Interfaz: %s\n", subIf.getName());
		}
	}

}